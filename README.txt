Prueba Técnica - NEXOS

Descripción del proyecto:

Debido a la naturaleza de los requerimientos planteados en la prueba, se optó por implementar una solución a través de 
un proyecto MVC en .NET framework 4.7.2, usando como ORM Entity Framework y una base de datosimpementada en SQLServer.

Por otra parte para el control de estilo y la visualización gráfica de la app web se usó Bootstrap 5.0

Finalmente es importante resaltar que a partir de los requerimientos presentados se tomó la decisión de usar dichas tecnologías 
mencionadas anterioremente y en especial .NET framework debido a su implementación flexible y mantenimiento sencillo. 



Archivos Adjuntos

Carpeta: img pruebas funcionales -> Contiene imagenes que evidencia la ejecucón de la pruebas funcionales
Caperta: nexos -> proyecto MVC.NET framework 4.7.2
Archivo: nexos.bak -> Comprimido de la BD para restaurar a través de SQLServer
Archivo: SQLQueryCreateNexos -> script en SQl que permite crear la BD y las tablas correspondientes


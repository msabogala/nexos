﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace nexos.Models.ViewModels
{
    public class AutorViewModel
    {
        public Guid IdAutor { get; set; }

        [Required]
        public string NombreCompleto { get; set; }

        [Required]
        public DateTime FechaNacimiento { get; set; }

        [Required]
        public string CiudadProcedencia { get; set; }

        [Required]
        public string Correo { get; set; }

        public List<AutorViewModel> AutorList { get; set; }
    }
}
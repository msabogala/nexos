﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace nexos.Models.ViewModels
{
    public class LibroViewModel
    {
        public Guid IdLibro { get; set; }

        [Required]
        public string Titulo { get; set; }
        [Required]
        public int Ano { get; set; }
        [Required]
        public string Genero { get; set; }
        [Required]
        public int NumeroPaginas { get; set; }
        public Guid IdAutor { get; set; }

        public string NombreAutor { get; set; }

        public List<LibroViewModel> LibroList { get; set; }
        public List<AutorViewModel> AutorList { get; set; }
    }
}
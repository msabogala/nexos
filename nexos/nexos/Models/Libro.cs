//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace nexos.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Libro
    {
        public System.Guid IdLibro { get; set; }
        public string Titulo { get; set; }
        public int Ano { get; set; }
        public string Genero { get; set; }
        public int NumeroPaginas { get; set; }
        public System.Guid IdAutor { get; set; }
    }
}

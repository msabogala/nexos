﻿using nexos.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using nexos.Models;
using nexos.Services;

namespace nexos.Controllers
{
    public class LibroController : Controller
    {
        private LibroService _libroService;
        private AutorService _autorService;
        public int MaxPermitido = 5;
        public LibroController()
        {
            _libroService = new LibroService();
            _autorService = new AutorService();
        }

        //GET
        public ActionResult RegistroLibro()
        {
            try
            {
                if (TempData["busqueda"] == null)
                {
                    LibroViewModel model = new LibroViewModel();
                    List<AutorViewModel> AutorList = new List<AutorViewModel>();

                    var data = _libroService.ConsultarLibros();
                    var autores = _autorService.ConsultarAutores();
                    if (autores != null)
                    {
                        model.AutorList = autores;
                    }
                    else
                    {
                        model.AutorList = AutorList;
                    }
                    if (data != null)
                    {
                        model.LibroList = data;
                        return View(model);
                    }
                    else
                    {
                        return View(model);
                    }
                }
                else
                {
                    LibroViewModel model = new LibroViewModel();
                    model = (LibroViewModel)TempData["busqueda"];
                    List<AutorViewModel> AutorList = new List<AutorViewModel>();
                    var autores = _autorService.ConsultarAutores();
                    if (autores != null)
                    {
                        model.AutorList = autores;
                    }
                    else
                    {
                        model.AutorList = AutorList;
                    }
                   
                    return View(model);
                }


            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult RegistroLibro(LibroViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    //No valido
                    return RedirectToAction("RegistroLibro", "Libro");
                }
                else
                {
                    var CantidadLibrosXUser = _libroService.ConsultarLibrosXAutor(model.IdAutor);
                    if(CantidadLibrosXUser < MaxPermitido)
                    {
                        var data = _libroService.RegistroLibro(model);
                        if (data == true)
                        {
                            return RedirectToAction("RegistroLibro", "Libro");
                        }
                        else
                        {
                            return RedirectToAction("RegistroLibro", "Libro");
                        }
                    }
                    else
                    {
                        
                        LibroViewModel _model = new LibroViewModel();
                        List<AutorViewModel> _AutorList = new List<AutorViewModel>();
                        var _autores = _autorService.ConsultarAutores();
                        var data = _libroService.ConsultarLibros();
                        if (_autores != null)
                        {
                            _model.AutorList = _autores;
                        }
                        else
                        {
                            _model.AutorList = _AutorList;
                        }
                        if (data != null)
                        {
                            _model.LibroList = data;
                            ViewBag.error = "error";
                            return View(_model);
                        }
                        else
                        {
                            return View(_model);
                        }
                        
                    }
              
                }
             
               
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult BuscarLibro(LibroViewModel  model)
        {
            LibroViewModel _model = new LibroViewModel();
            var buscar = _libroService.BuscarLibros(model.Titulo, model.Ano, model.NombreAutor);
            _model.LibroList = buscar;

            TempData["busqueda"] = _model;
            return RedirectToAction("RegistroLibro", "Libro");
        }
    }
}
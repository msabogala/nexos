﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using nexos.Models;
using nexos.Models.ViewModels;
using nexos.Services;

namespace nexos.Controllers
{

    public class AutorController : Controller
    {
        private AutorService _autorService;
        public AutorController()
        {
            _autorService = new AutorService();
        }

        //GET
        public ActionResult RegistroAutor()
        {
            
            AutorViewModel model = new AutorViewModel();
            var data = _autorService.ConsultarAutores();
            if(data != null)
            {
                model.AutorList = data;
                return View(model);
            }
            else
            {
                return View(model);
            }   
           
        }

        [HttpPost]
        public ActionResult RegistroAutor(AutorViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("RegistroAutor", "Autor");
                }
                else
                {
                    //Datos Validos
                    var data = _autorService.RegistroAutor(model);
                    if (data == true)
                    {
                        return RedirectToAction("RegistroAutor", "Autor");
                    }
                    else
                    {
                        return RedirectToAction("RegistroAutor", "Autor");
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
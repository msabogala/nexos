﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using nexos.Models;
using nexos.Models.ViewModels;

namespace nexos.Services
{
    public interface ILibroService
    {
        bool RegistroLibro(LibroViewModel model);
        List<LibroViewModel> ConsultarLibros();
        List<LibroViewModel> BuscarLibros(string Titulo, int Ano, string Autor);
        int ConsultarLibrosXAutor(Guid IdAutor);
    }

    public class LibroService: ILibroService
    {
        public bool RegistroLibro(LibroViewModel model)
        {
            try
            {
                using (nexosEntities1 db = new nexosEntities1())
                {
                    var libro = new Libro();
                    libro.IdLibro = Guid.NewGuid();
                    libro.Titulo = model.Titulo;
                    libro.Ano = model.Ano;
                    libro.Genero = model.Genero;
                    libro.NumeroPaginas = model.NumeroPaginas;
                    libro.IdAutor = model.IdAutor;

                    db.Libro.Add(libro);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex )
            {

                return false;
            }
        }

        public List<LibroViewModel> ConsultarLibros()
        {
            try
            {
                List<LibroViewModel> LibroList;
                using (nexosEntities1 db = new nexosEntities1())
                {
                    LibroList = (from l in db.Libro
                                 join a in db.Autor
                                 on l.IdAutor equals a.IdAutor
                                 select new LibroViewModel
                                 {
                                     IdLibro = l.IdLibro,
                                     Titulo = l.Titulo,
                                     Ano = l.Ano,
                                     Genero = l.Genero,
                                     NumeroPaginas = l.NumeroPaginas,
                                     NombreAutor = a.NombreCompleto
                                 }).ToList();
                }
                return LibroList;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<LibroViewModel> BuscarLibros(string Titulo, int Ano, string Autor)
        {
            try
            {
                List<LibroViewModel> LibroList;
                using (nexosEntities1 db = new nexosEntities1())
                {
                    LibroList = (from l in db.Libro
                                 join a in db.Autor
                                 on l.IdAutor equals a.IdAutor 
                                 where l.Titulo.Contains(Titulo) || l.Ano == Ano || a.NombreCompleto.Contains(Autor)
                                 select new LibroViewModel
                                 {
                                     IdLibro = l.IdLibro,
                                     Titulo = l.Titulo,
                                     Ano = l.Ano,
                                     Genero = l.Genero,
                                     NumeroPaginas = l.NumeroPaginas,
                                     NombreAutor = a.NombreCompleto
                                 }).ToList();
                }
                return LibroList;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public int ConsultarLibrosXAutor(Guid IdAutor)
        {
            try
            {
                int CantidadLibrosXAutor;
                List<LibroViewModel> LibroList = new List<LibroViewModel>();
                using (nexosEntities1 db = new nexosEntities1())
                {
                    LibroList = (from l in db.Libro where l.IdAutor == IdAutor
                                 select new LibroViewModel
                                 {
                                     IdLibro = l.IdLibro,
                                     Titulo = l.Titulo,
                                     Ano = l.Ano,
                                     Genero = l.Genero,
                                     NumeroPaginas = l.NumeroPaginas,
                                 }).ToList();
                }
                CantidadLibrosXAutor = LibroList.Count();
                return CantidadLibrosXAutor;
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
    }
}

﻿using nexos.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using nexos.Models;

namespace nexos.Services
{
    public interface IAutorService
    {
        bool RegistroAutor(AutorViewModel model);
        List<AutorViewModel> ConsultarAutores();
    }
    public class AutorService : IAutorService
    {
        public List<AutorViewModel> ConsultarAutores()
        {
            try
            {
                List<AutorViewModel> AutorList;
                using (nexosEntities1 db = new nexosEntities1())
                {

                    AutorList = (from x in db.Autor
                                 select new AutorViewModel
                                 {
                                     IdAutor = x.IdAutor,
                                     NombreCompleto = x.NombreCompleto,
                                     FechaNacimiento = x.FechaNacimiento,
                                     CiudadProcedencia = x.CiudadProcedencia,
                                     Correo = x.Correo
                                 }).ToList();
                }
                return AutorList;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        public bool RegistroAutor(AutorViewModel model)
        {
            try
            {
                using (nexosEntities1 db = new nexosEntities1())
                {
                    var autor = new Autor();
                    autor.IdAutor = Guid.NewGuid();
                    autor.NombreCompleto = model.NombreCompleto;
                    autor.FechaNacimiento = model.FechaNacimiento;
                    autor.CiudadProcedencia = model.CiudadProcedencia;
                    autor.Correo = model.Correo;

                    db.Autor.Add(autor);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
    }
}